#!/bin/bash

RB="\e[0m\e[1;31m"
GB="\e[0m\e[1;32m"
YB="\e[0m\e[1;33m"
BB="\e[0m\e[1;34m"
MB="\e[0m\e[1;35m"
CB="\e[0m\e[1;36m"
N="\e[0m"
D="\e[0m\e[2m"
B="\e[0m\e[1m"

LOG_FILE=/tmp/project.log

LOGGER() {
  echo -e "${YB}------------------------ ** END OF $2 ** ----------------------------${N}" >>$LOG_FILE
  case $1 in 
    INFO) 
      STAT_COLOR=${B}
      ;; 
    FAIL) 
      STAT_COLOR=${RB}
      ;;
    SUCC) 
      STAT_COLOR=${GB}
      ;;
    SKIP) 
      STAT_COLOR=${YB}
  esac 

  case $SERVICE_NAME in 
    MONGODB|RABBITMQ|MYSQL|REDIS)
      echo -e "${D}$(date +%F' '%T) ${STAT_COLOR}[$1] ${BB}[$SERVICE_NAME] ${B}${2}${N}"
      ;;
    NGINX) 
      echo -e "${D}$(date +%F' '%T) ${STAT_COLOR}[$1] ${MB}[$SERVICE_NAME] ${B}${2}${N}"
      ;;
    CATALOGUE|CART|USER|SHIPPING|DISPATCH|PAYMENT) 
      echo -e "${D}$(date +%F' '%T) ${STAT_COLOR}[$1] ${CB}[$SERVICE_NAME] ${B}${2}${N}"
      ;;
  esac
}


STAT() {
  case $1 in 
    SKIP) 
      LOGGER SKIP "$2"
      ;;
    0) 
      LOGGER SUCC "$2"
      ;; 
    *) 
      LOGGER FAIL "$2"
      ;;
  esac
}


SERVICE_NAME=MONGODB
LOGGER INFO "Starting Mongodb Setup"
echo '[mongodb-org-4.2]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/4.2/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc' >> /etc/yum.repos.d/mongodb-org-4.2.repo
STAT $? "Setting up yum repository"

yum install -y mongodb-org &>>$LOG_FILE
STAT $? "Installing Mongodb"

SERVICE_NAME=RABBITMQ 
LOGGER INFO "Starting Rabbitmq setup"
yum list installed | grep erlang &>/dev/null
case $? in
  0)
    STAT SKIP "Installing ErLang Package"
    ;;
  *)
    yum install https://packages.erlang-solutions.com/erlang/rpm/centos/7/x86_64/esl-erlang_22.2.1-1~centos~7_amd64.rpm -y &>>LOG_FILE 
    STAT $? "Installing ErLang Package"
    ;;
esac
curl -s https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.rpm.sh | sudo bash &>>$LOG_FILE
STAT $? "setting up yum repos"

yum install rabbitmq-server -y &>>$LOG_FILE
STAT $? "installing rabbitmq server"

systemctl enable rabbitmq-server 
systemctl start rabbitmq-server
STAT $? "Starting rabbitmq server"

SERVICE_NAME=MYSQL
LOGGER INFO "starting mysql setup"

yum remove mariadb-libs -y &>/dev/null
yum list installed | grep mysql-community-server &>/dev/null
case $? in 
  0) 
    STAT SKIP "Downloading MYSQL"
    ;;
  *) 
    LOGGER INFO "Downloading MYSQL"
    wget https://downloads.mysql.com/archives/get/p/23/file/mysql-5.7.28-1.el7.x86_64.rpm-bundle.tar  -O /tmp/mysql-5.7.28-1.el7.x86_64.rpm-bundle.tar &>>$LOG_FILE 
    STAT $? "Downloading MYSQL"
    cd /tmp

    tar -xf mysql-5.7.28-1.el7.x86_64.rpm-bundle.tar &>>$LOG_FILE
    STAT $? "Extracting mysql"
 
    yum install mysql-community-client-5.7.28-1.el7.x86_64.rpm \
            mysql-community-common-5.7.28-1.el7.x86_64.rpm \
            mysql-community-libs-5.7.28-1.el7.x86_64.rpm \
            mysql-community-server-5.7.28-1.el7.x86_64.rpm -y &>>$LOG_FILE
    STAT $? "Installing mysql Database"
    rm -rf mysql-5* *.rpm
    ;;
esac

systemctl enable mysqld &>>$LOG_FILE
systemctl start mysqld &>>$LOG_FILE
STAT $? "Starting mysql"

SERVICE_NAME=REDIS
LOGGER INFO "Starting Redis Setup"

 yum install epel-release yum-utils -y &>>$LOG_FILE
 STAT $? "Installing REDIS EPEL & YUM UTILS Package"

yum list installed | grep remi-release &>/dev/null
case $? in
0)
 STAT SKIP "setting up REDIS yum repos"
 ;;
*)
 yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y &>>$LOG_FILE
 STAT $? "setting up REDIS yum repos"
 yum-config-manager --enable remi &>>$LOG_FILE
 ;;
esac

 yum install redis -y &>>$LOG_FILE
 STAT $? "Installing REDIS"

systemctl enable redis &>>$LOG_FILE
systemctl start redis &>>$LOG_FILE
STAT $? "Starting Redis"





